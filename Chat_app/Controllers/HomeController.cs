﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Chat_app.Models;
using Chat_app.Helper;

namespace Chat_app.Controllers
{
    public class HomeController : Controller
    {

        private chat_appContext entities = new chat_appContext();

        public IActionResult Index()
        {
            var user = HttpContext.Session.GetObject<LoginSession>(CommonContanst.USER_SESSION);
            if (user != null)
            {
                ViewBag.UserId = user.UserName;
                return View();
            }
            return RedirectToAction("Login", "Login", new { area = "" });
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [HttpGet]
        public IActionResult GetQuestion(string id_from, string id_to)
        {
            var result = entities.Message.Where(x => (x.FromUser.Equals(id_from) && x.ToUser.Equals(id_to)) || (x.FromUser.Equals(id_to) && x.ToUser.Equals(id_from))).OrderBy(x => x.CreatedAt).ToList();
            return Ok(result);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
