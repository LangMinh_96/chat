﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chat_app.Helper;
using Chat_app.Models;
using Microsoft.AspNetCore.Mvc;

namespace Chat_app.Controllers
{
    public class LoginController : Controller
    {
        private chat_appContext entities = new chat_appContext();

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(Student student)
        {
            var result = entities.Student.Where(x => x.UserName.Equals(student.UserName)).FirstOrDefault();
            if (result != null)
            {
                if (result.Pass.Equals(student.Pass))
                {
                    var session = new LoginSession();
                    session.UserName = result.UserName;
                    session.RoleID = result.RoleId;
                    HttpContext.Session.SetObject(CommonContanst.USER_SESSION, session);
                    return Ok(new { status = 1, data = result });
                }
                else
                {
                    return Ok(new { status = 0, message = "Sai mật khẩu" });
                }
            }
            return Ok(new { status = -1, message = "Tài khoản không tồn tại..." });
        }
    }
}