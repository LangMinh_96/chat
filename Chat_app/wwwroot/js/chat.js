﻿"use strict";

let connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

connection.on("user_connected", function (data) {
    var id = $('#user_id').val();
    var list = $('.inbox_chat');
    let temp = data;
    list.html("");
    $(data.data).each(function (index, item) {
        if (item.userName !== id) {
            list.append('<div class="chat_list" id="id_' + item.userName + '" data-value="' + item.userName +'">' +
                '<div class= "chat_people" >' +
                '<div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>' +
                '<div class="chat_ib">' +
                '<h5>' + item.userName + '<span class="chat_date"></span></h5>' +
                '<p>' + item.lastMessage + '</p>' +
                '</div>' +
                '</div>' +
                '</div>');
        }
    });


    $('.chat_list').click(function () {
        var id = $(this).data("value");
        $(".inbox_chat").children().removeClass("active_chat");
        $(this).addClass('active_chat');
        $('#to_user').val(id);
        GetData(id);
    });
});

connection.on("sendmess", function (data) {
    var list = $('.msg_history');
    $('#messageInput').val('');
        list.append('<div class="outgoing_msg">' +
            '<div class= "sent_msg" >' +
            '<p>' + data.messages + '</p>' +
            '<span class="time_date">' + data.createdAt + '</span>' +
            '</div>' +
        '</div>');
    $('.msg_history').scrollTop($('.msg_history').get(0).scrollHeight);
});

connection.on("ReceiveMessage", function (data) {
    var user = $('#to_user').val();
    var list = $('.msg_history');
    if (data.fromUser === user) {
        list.append('<div class="incoming_msg">' +
            '<div class= "incoming_msg_img" > <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>' +
            '<div class="received_msg">' +
            '<div class="received_withd_msg">' +
            '<p>' + data.messages + '</p>' +
            '<span class="time_date">' + data.createdAt + '</span>' +
            '</div>' +
            '</div>' +
            '</div>');
        $('.msg_history').scrollTop($('.msg_history').get(0).scrollHeight);
    }
});

connection.start().catch(function (err) {
    return console.error(err.toString());
});

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";

    connection.invoke("Connect", { "UserName": $('#user_id').val() }).catch(function (err) {
        return console.error(err.toString());
    });
}

document.getElementById("sendButton").addEventListener("click", function (event) {
    connection.invoke("SendMessage", $('#user_id').val(), $('#to_user').val(), $('#messageInput').val()).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

function GetData(id) {
    var list = $('.msg_history');
    var id_user = $('#user_id').val();
    list.html("");
    $.ajax({
        url: "/Home/GetQuestion",
        type: "GET",
        data: { "id_from": id, "id_to": $('#user_id').val()},
        success: function (data) {

            $(data).each(function (index, item) {
                if (item.fromUser === id_user) {
                    //out coming message
                    list.append('<div class="outgoing_msg">' +
                        '<div class= "sent_msg" >' +
                        '<p>' + item.messages + '</p>' +
                        '<span class="time_date">' + item.createdAt + '</span>' +
                        '</div >' +
                        '</div>');
                } else {
                    //incoming message
                    list.append('<div class="incoming_msg">'+
                        '<div class= "incoming_msg_img" > <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>'+
                        '<div class="received_msg">'+
                        '<div class="received_withd_msg">' +
                        '<p>' + item.messages + '</p>' +
                        '<span class="time_date">' + item.createdAt + '</span>' +
                            '</div>'+
                        '</div>'+
                    '</div>');
                }
            });

            console.log(list.get(0).scrollHeight);

            list.scrollTop(list.get(0).scrollHeight);

        }, error: function (error) {

        }
    });
}
