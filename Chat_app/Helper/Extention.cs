﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat_app.Helper
{
    public class LoginSession
    {
        public string UserName { get; set; }
        public int RoleID { get; set; }
    }

    public static class CommonContanst
    {
        public static readonly string USER_SESSION = "USER_SESSION";
    }

    public static class Extention
    {
        public static void SetObject(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObject<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
