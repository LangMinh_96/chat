﻿using Chat_app.Helper;
using Chat_app.Models;
using Chat_app.Models.HubModel;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat_app.Hubs
{
    public class ChatHub : Hub
    {
        private chat_appContext entities = new chat_appContext();

        public async Task Connect(Student student)
        {
            var result = ListUser.list.Where(x => x.UserName.Equals(student.UserName)).FirstOrDefault();
            
            if (result == null)
            {
                ListUser.list.Add(new InfomationHub() { ConnecttionId = Context.ConnectionId, UserName = student.UserName, RoleId = 0 });
            }
            await Clients.Caller.SendAsync("connected", new { status = true, data = student });
            await Clients.All.SendAsync("user_connected", new { status = true, data = ListUser.list });
        }

        public async Task SendMessage(string from_user, string to_user, string message)
        {
            var to = ListUser.list.Where(x => x.UserName.Equals(to_user)).FirstOrDefault();
            try
            {
                Message mess = new Message()
                {
                    FromUser = from_user,
                    ToUser = to_user,
                    Messages = message,
                    CreatedAt = DateTime.Now
                };
                entities.Message.Add(mess);
                entities.SaveChanges();
                await Clients.Caller.SendAsync("sendmess", mess);
                if (to != null)
                {
                    await Clients.Client(to.ConnecttionId).SendAsync("ReceiveMessage", mess);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            var id = ListUser.list.Where(x => x.ConnecttionId.Equals(Context.ConnectionId)).FirstOrDefault();
            if (id != null)
            {
                ListUser.list.Remove(id);
            }

            Clients.All.SendAsync("user_connected", new { status = true, data = ListUser.list });

            return base.OnDisconnectedAsync(exception);
        }

    }
}
