﻿using System;
using System.Collections.Generic;

namespace Chat_app.Models
{
    public partial class Role
    {
        public Role()
        {
            Student = new HashSet<Student>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public ICollection<Student> Student { get; set; }
    }
}
