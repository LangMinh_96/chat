﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chat_app.Models;

namespace Chat_app.Models.HubModel
{
    public class InfomationHub : Student
    {
        private chat_appContext entities = new chat_appContext();

        public string ConnecttionId { get; set; }
        public string LastMessage
        {
            get
            {
                var user = entities.Student.Where(x => x.UserName.Equals(this.UserName)).FirstOrDefault();
                if (user != null)
                {
                    var message = entities.Message.Where(x => x.ToUser.Equals(user.UserName)).OrderByDescending(x => x.CreatedAt).FirstOrDefault();
                    return message != null ? message.Messages : "";
                }
                else
                    return "";
            }
        }
    }
}
