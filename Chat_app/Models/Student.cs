﻿using System;
using System.Collections.Generic;

namespace Chat_app.Models
{
    public partial class Student
    {
        public Student()
        {
            MessageFromUserNavigation = new HashSet<Message>();
            MessageToUserNavigation = new HashSet<Message>();
        }

        public string UserName { get; set; }
        public string Pass { get; set; }
        public string FullName { get; set; }
        public int RoleId { get; set; }
        public DateTime CreateAt { get; set; }

        public Role Role { get; set; }
        public ICollection<Message> MessageFromUserNavigation { get; set; }
        public ICollection<Message> MessageToUserNavigation { get; set; }
    }
}
