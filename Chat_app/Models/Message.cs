﻿using System;
using System.Collections.Generic;

namespace Chat_app.Models
{
    public partial class Message
    {
        public int MessageId { get; set; }
        public string FromUser { get; set; }
        public string ToUser { get; set; }
        public string Messages { get; set; }
        public DateTime CreatedAt { get; set; }

        public Student FromUserNavigation { get; set; }
        public Student ToUserNavigation { get; set; }
    }
}
