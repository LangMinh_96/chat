﻿using System;
using System.Collections.Generic;

namespace Chat_app.Models
{
    public partial class HighlightQuestion
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string StudentName { get; set; }
        public string QuestionContent { get; set; }
    }
}
